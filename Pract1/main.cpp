#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include "search.h" 
#include <locale.h>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

/*
* test0 ���������� ������
* test11 ��� ���� ������ ������
* test12 �� ������� ��������
* test13 ���� �������
* test1 ������������ �������� (10� ������)
* test2 ������������ �������� (20� ������)
* test3 ������������ �������� (30� ������)
* test4 ������������ �������� (40� ������)
* test5 ������������ �������� (50� ������)
* test6 ������������ �������� (60� ������)
* test7 ������������ �������� (70� ������) 
* test8 ������������ �������� (80� ������) 
* test9 ������������ �������� (90� ������) 
* test10 ������������ �������� (100� ������)
*/

char* generateRandomCharacters(int length) {
	const char characters[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	char* randomString = new char[length + 1];

	for (int i = 0; i < length; ++i) {
		int randomIndex = rand() % (sizeof(characters) - 1);
		randomString[i] = characters[randomIndex];
	}

	randomString[length] = '\0'; // ��������� ������� ������ � ����� ������

	return randomString;
}

void generateCSV(const char* csvFile, int rows) {

	FILE* file = fopen(csvFile, "w");

	if (file == NULL) {
		printf("�� ������� ������� ����.");
		return;
	}
	srand(time(0)); // �������������� ��������� ��������� ����� � ������� �������� �������

	for (int i = 0; i < rows; ++i) {
		int value1 = (rand() % 8)+4; // ��������� �������� ��� ������� �������
		int value2 = (rand() % 8)+4; // ��������� �������� ��� ������� �������

		char* randomString1 = generateRandomCharacters(value1); // ��������� ��������� ������ ������ 5 ��������
		char* randomString2 = generateRandomCharacters(value2); // ��������� ��� ����� ��������� ������ ������ 5 ��������
		fprintf(file, "%s;%s\n", randomString1, randomString2 ); // ���������� �������� � ����
		delete[] randomString1; // ����������� ������, ���������� ��� ����������� �������
		delete[] randomString2;
	}

	fclose(file);
	printf("���� ������� ������.");
}

int main() {
	//�����������
	setlocale(LC_ALL, "Russian");
	char resultValue[256];
	//������� �� "serch.cpp"
	int result = 0;
	char ch;

	//���������� ���������� ��� ������
	time_t start;
	time_t stop;
	double time;
	char csvFile[100];
	char searchKey[100];
	do {
		system("cls");
		printf("����:\n");
		printf("1 - �������� ������ �� �����\n");
		printf("2 - �������� �������� �� �����\n");
		printf("3 - �������� �������� �� �������\n");
		printf("4 - ������������� ������(csv ����)\n");

		printf("ecs - �����\n");
		ch = _getch();

		switch (ch) {
		case '1':					
			system("cls");

			printf("������� ��� �����: ");
			scanf("%99s", csvFile);

			printf("������� ������: ");
			scanf("%99s", searchKey);
			start = clock();                //����� �� ����������
			result = search(csvFile, searchKey, resultValue, -1);
			stop = clock();             //����� ����� ����������-
			time = (stop - start) / 1000.0;    //����� ����������
			printf("\n");
			printf("����� ���������� ����������: ");
			printf("%lf\n", time);

			if (result == -1)
			{
				printf("������: �������� �� �������.\n");

			}

			else if (result == -2)
			{
				printf("������: �� ���������� ������ ������.\n");

			}

			else {
				printf("�������� �������, ������ = %d.\n", result);

			}

			break;

		case '2':												
			system("cls");

			printf("������� ��� �����: ");
			scanf("%99s", csvFile);

			printf("������� ������: ");
			scanf("%99s", searchKey);
			start = clock();                //����� �� ����������
			result = search(csvFile, searchKey, resultValue, -1);

			stop = clock();             //����� ����� ����������
			time = (stop - start) / 1000.0;    //����� ����������
			printf("\n");
			printf("����� ���������� ����������: ");
			printf("%lf\n", time);

			if (result == -1)
			{
				printf("������: �������� �� �������.\n");

			}

			else if (result == -2)
			{
				printf("������: �� ���������� ������ ������.\n");

			}

			else {
				printf("�������� �������: %s \n", resultValue);
			}
			break;

		case'3':
			system("cls");
			printf("������� ��� �����: ");
			scanf("%99s", csvFile);
			printf("������� ������-: ");
			int id;
			scanf("%d", &id);
			start = clock();                //����� �� ����������
			result = search(csvFile, searchKey, resultValue, id);
			stop = clock();             //����� ����� ����������-
			time = (stop - start) / 1000.0;    //����� ����������
			printf("\n");
			printf("����� ���������� ����������: ");
			printf("%lf\n", time);

			if (result == -1)
			{
				printf("������: �������� �� �������.\n");

			}

			else if (result == -2)
			{
				printf("������: �� ���������� ������ ������.\n");

			}

			else {
				printf("�������� �������: %s \n", resultValue);

			}

			break;

		case'4':
			system("cls");
			printf("������� ��� �����: ");
			scanf("%99s", csvFile);

			printf("������� ���������� ��������� �������: ");
			int size;
			scanf("%d", &size);
			printf("\n");
			generateCSV(csvFile,size);
			printf("������������� ������\n");

			break;
		default:
			printf("������������ ������� ");
			break;
		}
		system("pause");
	} while (ch != 27);



	return 0;
}