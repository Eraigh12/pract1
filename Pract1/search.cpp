#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "search.h"
#include <string.h>




// ������� ��� ������ �������� � ��������������� �������
int search(const char* csvFile, const char* searchKey, char* searchValue, const int id) {
	// ��������� ����
	FILE* file = fopen(csvFile, "r");
	if (file == NULL) {
		printf("������ ��� �������� �����.\n");
		return -1;
	}

	// ��������� ���� ���������
	char line[256];
	int i = 0;
	while (fgets(line, sizeof(line), file)) {
		// ���������� ������� � �������� �����������
		char* token = strtok(line, ";");
		if (id == i) {
			token = strtok(NULL, ";");
			if (token == NULL || strcmp(token, "\n") == 0) {
				i = -2;
			}
			strcpy(searchValue, token);
			fclose(file);
			return i; // �������� �������
		}
		if (id == -1) {
			for (int j = 0; token != NULL;j++) {
				// ���������, ��������� �� �������� � �������
				if (strcmp(token, searchKey) == 0 && strlen(token) == strlen(searchKey)) {
					token = strtok(NULL, ";");
					if (token == NULL || strcmp(token, "\n") == 0) {
						i = -2;
					}
					strcpy(searchValue, token);
					fclose(file);
					return i; // �������� �������
				}
				token = strtok(NULL, ";");
				if (j > 0 && token != NULL) {
					fclose(file);
					return -2; // ������ ������ �� �����
				}
			}
		}
		i++;
	}

	fclose(file);
	return -1; // �������� �� �������
}